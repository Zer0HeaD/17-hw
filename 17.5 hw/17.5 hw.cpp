#include <iostream>
#include <time.h>
#include <math.h>
using namespace std;

class Vector
{
private:
	double x;
	double y;
	double z;
	double res;

public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void root()
	{	
		res = x * x + y * y + z * z;
		sqrt(res);
		cout << res << endl;
	}
	void Show()
	{
		cout << endl << x << ' ' << y << ' ' << z << endl;
	}
};

int main()
{
	Vector v(5, 3, 10);
	v.Show();
	v.root();
}